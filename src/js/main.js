//= ../../node_modules/jquery/dist/jquery.min.js
//= ../../node_modules/tooltipster/dist/js/tooltipster.bundle.min.js
//= ../../node_modules/mobile-detect/mobile-detect.min.js
//= ../../node_modules/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js
//= ../../node_modules/accordion-js/src/accordion.js
//= ../../node_modules/jquery-modal/jquery.modal.min.js
//= ../../node_modules/jquery-validation/dist/jquery.validate.min.js
//= ../../node_modules/jquery-mask-plugin/dist/jquery.mask.js
//= ../../node_modules/resize-sensor/ResizeSensor.min.js
//= ../../node_modules/sticky-sidebar/dist/jquery.sticky-sidebar.min.js
//= ../../node_modules/swiper/js/swiper.min.js

//= page-slider.js


$(document).ready(() => {
    $('#navHamburger').click(() => {
        $('#navHamburger').toggleClass('is-active');
        $('#headerNav').toggleClass('is-active');
        $('.nav-cover').toggleClass('is-active');
    });

    $('.tooltip').tooltipster({
        theme: ['tooltipster-noir', 'tooltipster-noir-customized'],
        animation: 'fade',
        delay: 200,
        trigger: "custom",
        arrow: false,
        interactive: true,
        functionInit: function (instance, helper) {
            let $origin = $(helper.origin),
                dataOptions = $origin.attr('data-tooltipster');
            if (dataOptions) {
                dataOptions = JSON.parse(dataOptions);
                $.each(dataOptions, function (name, option) {
                    instance.option(name, option);
                });
            }
        },
        triggerOpen: {
            mouseenter: true,
            tap: true,
        },
        triggerClose: {
            mouseleave: true,
            tap: true,
            scroll: true
        }
    });

    $('.cart-tooltip').hide();
    $('.cart-holder').click(() => {
        $('.cart-tooltip').toggle();
    });

    //number input
    $('.number-input__minus').click(() => {
        let $input = $(this).parent().find('input');
        let count = parseInt($input.val()) - 1;
        count = count < 1 ? 1 : count;
        $input.val(count);
        $input.change();
        return false;
    });
    $('.number-input__plus').click(() => {
        let $input = $(this).parent().find('input');
        $input.val(parseInt($input.val()) + 1);
        $input.change();
        return false;
    });
    //custom scrollbar

    $(window).on("load", () => {
        let md = new MobileDetect(window.navigator.userAgent);

        $.mCustomScrollbar.defaults.theme = 'dark-3';
        $('.order-items-holder').mCustomScrollbar();
        $('.friends__items').mCustomScrollbar({
            axis: 'x',
        });

        if (!md.mobile()) {
            $('.faq__content').mCustomScrollbar();

            $('#stickySidebar').stickySidebar({
                topSpacing: 0,
                bottomSpacing: 0,
                resizeSensor: true,
                containerSelector: '.product-holder',
                innerWrapperSelector: '.properties-inner'
            });
        } else {
            if ($('.faq__content').length){
                new Accordion(['.faq__content'], {duration: 500,});
            }
            if ($('.product-holder__img, .reviews').length){
                var mySwiper = new Swiper ('.product-holder__img, .reviews', {
                    loop: true,
                    speed: 600,
                    pagination: {
                        el: '.swiper-pagination',
                    },
                    autoplay: {
                        delay: 2000,
                    }
                })
            }
        }

        $(".faq__nav").on("click", "a[href^='#']", function (e) {
            $('.faq__nav a').removeClass('selected');
            let href = $(this).attr("href"), target = $(href).parents(".mCustomScrollbar");
            if (target.length) {
                e.preventDefault();
                target.mCustomScrollbar("scrollTo", href);
            }
            $(this).addClass('selected')
        });

    });
    //modals
    $('a.open-modal').click(function (event) {
        $(this).modal({
            fadeDuration: 250
        });
        return false;
    });

    //validate forms
    let requiredMsg = "Это поле обязательно";
    let ratingMsg = "Ваша оценка важна для нас";
    let textareaMsg = "Напишите пару строк, это же не сложно";
    $('#addReview').validate({
        wrapper: "div",
        rules: {
            rating: {
                required: true,
            },
            email: {
                required: true,
                email: true
            },
            textarea: {
                required: true,
            }
        },
        errorPlacement: (error, element) => {
            if (element.is(":radio")) {
                error.appendTo('.error-container');
            } else {
                error.insertAfter(element);
            }
        },
        messages: {
            rating: ratingMsg,
            email: requiredMsg,
            textarea: textareaMsg,
        },
    });
    $('#addReviewBtn').prop('disabled', 'disabled');
    $('#addReview input').on('blur', function() {
        if ($("#addReview").valid()) {
            $('#addReviewBtn').prop('disabled', false);
        } else {
            $('#addReviewBtn').prop('disabled', 'disabled');
        }
    });
    $('#reportReceipt').validate({
        wrapper: "div",
        rules: {
            email: {
                required: true,
                email: true
            },
            messages: {
                email: requiredMsg,
            },
        },
    });

});

let year = new Date().getFullYear();
$('#year').html(year);

let el = document.getElementById('slider');

if (el) {
    var mySlider = slider('.vertical-carousel');
}
